#!homework assignment lpic25/ssh


#2 check sshd protocol

cd /etc/ssh --> grep protocol sshd_config

#3 start ssh server and show the directory

ssh student@10.0.1.15 pwd

#4 copy a file to server

scp ~/serverhosts student@10.0.1.15:/etc/hosts.new 

#5 copy a file from server to my computer

scp student@10.0.1.15:/etc/hosts /home/user/serverhosts

#6 use ssh to run xeyeg

ssh -X user@127.0.0.1

#7 create ssh keygen and copy to another user ssh.pub

ssh-keygen -t rsa

scp id_rsa.pub user@127.0.0.1:~/.ssh/authorized_keys

#8 verify the permission on the server key is corrected

ls -i /etc/ssh/ssh_host_*

#9 verify that ssh-agent is running

ps -ef| grep ssh-agent

#10 protect your keypair with a passphrase

ssh user@127.0.0.1 "ls -i .ssh"

ssh-add -L
ssh -v user@127.0.0.1



